# upmail

## Requirement 
 1. [Docker](https://www.docker.com)  : make sure Docker is installed in system.

 2.  Clone Project in local system

## Starting Development server

1. project is designed to work in local with Docker single command

   `docker-compose up`
  
   fire this command from project root
2. this will start following service
   1. [http://127.0.0.1:8000/login/](http://127.0.0.1:8000/login/) is django development server
   2. [http://127.0.0.1:9090/](http://127.0.0.1:9090) prometheus server for django metrics and analysis data pull
   3. [http://127.0.0.1:3000/](http://127.0.0.1:3000) Grafana server for creating Realtime dashboard and analytics
   4. Redis  server for Django Q service as we will be having fetching mails background task
   5. Postgress - database 

### Configuring Google oauth
   1. visit [http://127.0.0.1:8000/admin/](http://127.0.0.1:8000/admin/)
   2. use `username/password : admin/admin`
   3. first we need to add our host intp site 

       visit `[http://127.0.0.1:8000/admin/sites/site/1/change/](http://127.0.0.1:8000/admin/sites/site/1/change/)`
       and modify Domain name to `127.0.0.1:8000`
   4. Adding Social app
    
        visit [http://127.0.0.1:8000/admin/socialaccount/socialapp/add/](http://127.0.0.1:8000/admin/socialaccount/socialapp/add/)
        
        select Provider = `Google`

        Client id / Secret key  get these from Google console 
        Sites :- select the host that we created in site model
        
        use [https://dev.to/mdrhmn/django-google-authentication-using-django-allauth-18f8](https://dev.to/mdrhmn/django-google-authentication-using-django-allauth-18f8) if your facing any difficulty. 

   finally visit [http://127.0.0.1:8000/login/](http://127.0.0.1:8000/login/) for login.  Google button will be visible.
   click the button and select email which is you have added under the Google Cloud testing user other wise it will show error like developer is not verified.
   
    if loggin success you will be redirected to home page and mail sync service will start in backgraound.


### Building Grafana For monitoring django :
1. visit [http://127.0.0.1:3000/](http://127.0.0.1:3000/) and login to grafana with `username/password : admin/foobar`
2. after login click on Setting icon on let bottom and select Data Source
3. Search for Prometheus and select Prometheus
4. it will open config page. paste `http://prometheus:9090` in  URL filed.
5. click on `save and test `. our data source is linked to garafana,
6. now click on Dashboard -> Import
7. Paste `9528` in  dashboard url 
8. Click Load and then Import
9. A beautiful dashboard will be visible 


### Architecture Digram

![Alt text](readme/architecture.png?raw=true "Title")


### Api End Point 

1. DRF mail pagination api 
        
         DRF TOKEN Can be found on home page of website in right top side `Copy Token` button.

   `GET  '127.0.0.1:8000/get_messages/'`

   Header params:

    `Authorization: TOKEN <token>` 

    Response

    ```
   {
    "count": 15730,
    "next": "http://127.0.0.1:8000/get_messages/?limit=50&offset=50",
    "previous": null,
    "results':[
     {
            "id": "182b00432675ee95",
            "user": null,
            "thread_id": "182b00432675ee95",
            "label_ids": "UNREAD,CATEGORY_UPDATES,INBOX",
            "snippet": "This is a system generated mail so please do not reply to this mail. Click here to unsubscribe Disclaimer: State Bank never sends e-mails and embedded links asking you to update or verify confidential,",
            "size_estimate": "6316",
            "history_id": "3839795",
            "internal_date": "1660810441000",
            "payload": {
                "body": {
                    "data": "PGh0bWw-PGJvZHk-PHRhYmxlPiA8dGJvZHk-IDx0cj4gPHRkIGFsaWduPSJjZW50ZXIiPiA8aW1nIHNyYz0iaHR0cHM6Ly93d3cuc2JpeW9uby5zYmkvd3BzL3djbS9jb25uZWN0L2MyMDM4ZTQ3LWM0MjYtNGI5MS04NWFhLWVmYzY3MDAzMzRjMi9ZV0NNODMucG5nP01PRD1BSlBFUkVTIiAvPiA8L3RkPiA8L3RyPiA8dHI-IDx0ZD4gPHA-VGhpcyBpcyBhIHN5c3RlbSBnZW5lcmF0ZWQgbWFpbCBzbyBwbGVhc2UgZG8gbm90IHJlcGx5IHRvIHRoaXMgbWFpbC4gPHA-Q2xpY2sgPGEgaHJlZj0iaHR0cHM6Ly9kZWxpdmVyeWFsZXJ0cy5zYmkuY28uaW4vR0haRk1QREJJUVM_aWQ9MTk4PVpVUURWUUVHVUZoYkhoRVRSUmRHUXhFVUdCSVFGQlJGR1VVWEVCVVFSRUlRR0JORkVSWkJRVU5DRVJORkYwWkRFUVJFV0ZsQ1ZRc0pVUWNFUlZFUUIxeDRWQWhRWHcxUEFBMWNUMVFCVUZNR0Fna0JBQVVDVUE1ZEJ3WUVUQXdXUkVoQVh4NFpCUVFQQzBkV0YwNEhEMVJHVEVFZVIxWU1Gd1pZSGx4ZVN5VjRZblVvWVhJaktESXhEbG9CU3hCUSI-aGVyZTwvYT4gdG8gdW5zdWJzY3JpYmU8L3A-DQo8L3A-IDxwPkRpc2NsYWltZXI6IFN0YXRlIEJhbmsgbmV2ZXIgc2VuZHMgZS1tYWlscyBhbmQgZW1iZWRkZWQgbGlua3MgYXNraW5nIHlvdSB0byB1cGRhdGUgb3IgdmVyaWZ5IGNvbmZpZGVudGlhbCwgcGVyc29uYWwgYW5kIHNlY3VyaXR5IGRldGFpbHMuIElmIHlvdSByZWNlaXZlIHN1Y2ggZW1haWxzL3Bob25lIGNhbGxzL1NNUywgTkVWRVIgUkVTUE9ORCB0byB0aGVtIGFuZCByZXBvcnQgc3VjaCBtYXR0ZXIgdG8gdGhlIGJhbmsgYXQgcmVwb3J0LnBoaXNoaW5nQHNiaS5jby5pbjwvcD4gPC90ZD4gPC90cj4gPC90Ym9keT4gPC90YWJsZT48aW1nIGlkPSdTQkJCQicgc3JjPSdodHRwczovL2RlbGl2ZXJ5YWxlcnRzLnNiaS5jby5pbi9HSFpGTVBEQklRUz9pZD0xOTg9ZjBRRFZRRUdVRmhiSGhFVFJSZEdReEVVR0JJUUZCUkZHVVVYRUJVUVJFSVFHQk5GRVJaQlFVTkNFUk5GRjBaREVRUkVXRmxDVlFzSlVRY0VSVkVRQjF4NFZBaFFYdzFQQUExY1QxUUJVRk1HQWdrQkFBVUNVQTVkQndZRVRBd1dSRWhBWHg0WkJRUVBDMGRXRjA0SEQxUkdURUVlUjFZTUZ3WllIbHhlU3lWNFluVW9ZWElqS0RJeERsb0JTeEJRJyAvPjwvYm9keT48L2h0bWw-DQo=",
                    "size": 1148
                },
                "partId": "",
                "headers": [
                    {
                        "name": "Delivered-To",
                        "value": "jivan0404patel@gmail.com"
                    },
                    {
                        "name": "Received",
                        "value": "by 2002:a2e:95c3:0:0:0:0:0 with SMTP id y3csp100565ljh;        Thu, 18 Aug 2022 01:14:01 -0700 (PDT)"
                    },
                    {
                        "name": "X-Google-Smtp-Source",
                        "value": "AA6agR47n9inYIdOEefzpBVSLIGNScGVMbvbj9IgvtaJ+Fqd/7qK3hojuGH75A1TEL2UP9LksWUP"
                    },
                    {
                        "name": "X-Received",
                        "value": "by 2002:a17:90b:2c42:b0:1fa:c295:3547 with SMTP id rw2-20020a17090b2c4200b001fac2953547mr4076349pjb.236.1660810441419;        Thu, 18 Aug 2022 01:14:01 -0700 (PDT)"
                    },
                    {
                        "name": "ARC-Seal",
                        "value": "i=1; a=rsa-sha256; t=1660810441; cv=none;        d=google.com; s=arc-20160816;        b=OUDO70oK8ic3YzrSJq7rn1O3LjO/qS+EEal19xPjj5DVKXHv64NGZJNoWdWAuHEQEb         61FNSLUNOa3Xg/0SABd9GSf4yRTl5WJ8TEa6NHWsKG5zc5Dgq7lsr57sEfPuIRuEyh7b         bQ8VOt1offhhYu0RHxI9/0pQgtT+8htOh2/cQrAo5HMAQgYayi03TidJaQTLd+9nvPnk         uRKIwj5LDJAXDrM5wGsAYnWf4oL840wAA7cwn7xKs0k6ze9MCVDI5fXbizRY5hv27CSD         MbzhFKKZXQorCOZX7l2Q7p0Zu/35e77vlhfBd1YRHAbrTUtYzD27D8nbgHX+g4428gfB         o18A=="
                    },
                    {
                        "name": "ARC-Message-Signature",
                        "value": "i=1; a=rsa-sha256; c=relaxed/relaxed; d=google.com; s=arc-20160816;        h=date:feedback-id:list-unsubscribe:content-transfer-encoding         :mime-version:subject:message-id:to:reply-to:from:dkim-signature         :dkim-signature;        bh=D0dUBi2N8wZ31xrDUE6U+/SwLEfxy5FtVDo4+WmlEzk=;        b=Z/FlI1+ktaZ3vu/L5FOBo0xZ9v742yK/cRnBRqs0XQySjXyuIZxHAhKhEKoMXK+P5P         0MzjanMJOttyJbt2vg/uRmnTO394A175rSW7PLK3hAxKI8W0mnt8quOZTjmRrn2Rc0TJ         MGbZM35/Ez+mbB6r1/UXZDNz5e73FAD7tvR4tP9AY6KXc7CoC+Ho1cBiL1j2ZUMqgSIP         FjRUts0L1mUGt/J9LZohhFhYijhHA2Y8MdslZNWrLYzSNT7cAxtUYqh2Ak5rLX3Y+oJP         wrgkEDRs8BcR++mtrbYVk4gAz8CTA28r6xvpOunmBYv84VFto/0CkTqgKUazkVbaRB+f         wsCg=="
                    },
                    {
                        "name": "ARC-Authentication-Results",
                        "value": "i=1; mx.google.com;       dkim=pass header.i=@alerts.sbi.co.in header.s=nc header.b=Tl8a1xkL;       dkim=pass header.i=@env.etransmail.com header.s=fnc header.b=\"FuO//PNG\";       spf=pass (google.com: domain of 16607613016578061-198-1-gmail.com@alerts.sbi.co.in designates 175.158.69.51 as permitted sender) smtp.mailfrom=16607613016578061-198-1-gmail.com@alerts.sbi.co.in;       dmarc=pass (p=REJECT sp=REJECT dis=NONE) header.from=sbi.co.in"
                    },
                    {
                        "name": "Return-Path",
                        "value": "<16607613016578061-198-1-gmail.com@alerts.sbi.co.in>"
                    },
                    {
                        "name": "Received",
                        "value": "from d51-smtp-out-in.communications.sbi.co.in (d51-smtp-out-in.communications.sbi.co.in. [175.158.69.51])        by mx.google.com with ESMTPS id a19-20020a056a000c9300b0052f65759ad5si1096898pfv.143.2022.08.18.01.14.00        for <jivan0404patel@gmail.com>        (version=TLS1_2 cipher=ECDHE-ECDSA-AES128-GCM-SHA256 bits=128/128);        Thu, 18 Aug 2022 01:14:01 -0700 (PDT)"
                    },
                    {
                        "name": "Received-SPF",
                        "value": "pass (google.com: domain of 16607613016578061-198-1-gmail.com@alerts.sbi.co.in designates 175.158.69.51 as permitted sender) client-ip=175.158.69.51;"
                    },
                    {
                        "name": "Authentication-Results",
                        "value": "mx.google.com;       dkim=pass header.i=@alerts.sbi.co.in header.s=nc header.b=Tl8a1xkL;       dkim=pass header.i=@env.etransmail.com header.s=fnc header.b=\"FuO//PNG\";       spf=pass (google.com: domain of 16607613016578061-198-1-gmail.com@alerts.sbi.co.in designates 175.158.69.51 as permitted sender) smtp.mailfrom=16607613016578061-198-1-gmail.com@alerts.sbi.co.in;       dmarc=pass (p=REJECT sp=REJECT dis=NONE) header.from=sbi.co.in"
                    },
                    {
                        "name": "DKIM-Signature",
                        "value": "v=1; a=rsa-sha256; c=relaxed/relaxed; s=nc; d=alerts.sbi.co.in; h=From:Reply-To:To:Message-ID:Subject:MIME-Version:Content-Type:Content-Transfer-Encoding:List-Unsubscribe:Date; i=yonosbi@alerts.sbi.co.in; bh=D0dUBi2N8wZ31xrDUE6U+/SwLEfxy5FtVDo4+WmlEzk=; b=Tl8a1xkLHw0Qt/S3m+nLlZhSNeZQuwt3tu4utyGKdlYo0dqFtoZs/Kd2OixK4PVkHZkTjWkEVl77   JYA4/MUPkeAqo2NFqi5k17RQsR8/+fUHzUo2vZmUwUuMyDEMwrJ/qjfDjKFB9fD7DzfFSgjF1VzZ   v/iESFCTaUHwA3As/k5KBkfYJgWxnbDFaJfDPgvU6vjeV3K2Aooa7b22V1vH8ljeWC8RCP7tur+C   FCmPWOgfS2wI2yRv7uDvl4UHasJJyHPO/0w13xCCuD1S2HNlvkIk+VFdim/AjymSWjXlJgNHcRpc   MC/ByWf0VX9dBA5FUbe/hAWOeIgtSKleRFyl9A=="
                    },
                    {
                        "name": "DKIM-Signature",
                        "value": "v=1; a=rsa-sha256; c=relaxed/relaxed; s=fnc; d=env.etransmail.com; h=From:Reply-To:To:Message-ID:Subject:MIME-Version:Content-Type:Content-Transfer-Encoding:List-Unsubscribe:Date; bh=D0dUBi2N8wZ31xrDUE6U+/SwLEfxy5FtVDo4+WmlEzk=; b=FuO//PNGfv3QPJadFL1PFfrs4GA+xn3a5Tm6BQDvSPUHcJBS9k438QnjGwvNZ7HEgENMYbkCetK5   TBNnK+UjdMnzVB1L7DscMq9pLNniCbIsMch7YFSaP/9uZAXjTzYJK+sWzuKDEULa2iVCceI0scSE   NJ7Fb8DRIpnf8n4f4rk="
                    },
                    {
                        "name": "From",
                        "value": "YONO SBI <yonosbi@alerts.sbi.co.in>"
                    },
                    {
                        "name": "Reply-To",
                        "value": "YONO SBI <yonosbi@alerts.sbi.co.in>"
                    },
                    {
                        "name": "To",
                        "value": "jivan0404patel@gmail.com"
                    },
                    {
                        "name": "Message-ID",
                        "value": "<1268143696.24018348.1660810437749@Dig-ELKLOG>"
                    },
                    {
                        "name": "Subject",
                        "value": "Transfer funds between accounts seamlessly with YONO"
                    },
                    {
                        "name": "MIME-Version",
                        "value": "1.0"
                    },
                    {
                        "name": "Content-Type",
                        "value": "text/html; charset=us-ascii"
                    },
                    {
                        "name": "Content-Transfer-Encoding",
                        "value": "7bit"
                    },
                    {
                        "name": "X-InjTime",
                        "value": "1660810437"
                    },
                    {
                        "name": "List-Unsubscribe",
                        "value": "<mailto:16607613016578061@alerts.sbi.co.in?subject=Unsubscribe>, <https://deliveryalerts.sbi.co.in/GHZFMPDBIQS?id=198=ZUQDVQEGUFhbHhETRRdGQxEUGBIQFBRFGUUXEBUQREIQGBNFERZBQUNCERNFF0ZDEQREWFlCVQsJUQcERVEQB1x4VAhQXw1PAA1cT1QBUFMGAgkBAAUCUA5dBwYETAwWREhAXx4ZBQQPC0dWF04HD1RGTEEeR1YMFwZYHlxeSyV4YnUoYXIjKDIxDloBSxBQ>"
                    },
                    {
                        "name": "Feedback-ID",
                        "value": "MTk4OjIwMjIwODE4XzEzOg==:pepipostE"
                    },
                    {
                        "name": "X-FNCID",
                        "value": "198-16607613016578061-0"
                    },
                    {
                        "name": "X-Traffic-Type",
                        "value": "198-2"
                    },
                    {
                        "name": "Date",
                        "value": "Thu, 18 Aug 2022 13:44:01 +0530"
                    }
                ],
                "filename": "",
                "mimeType": "text/html"
            }
        },
   ]
    }
   ```


### Error Log Middleware 
    Middleware
    `custom_logging.middleware.ExceptionLogMiddleware`
    
    congif list in settings.py:
    CUSTOM_LOGGING_ERROR_CODE = [302,500,<your error code>]
    
    Testing Api End Point
    GET : http://127.0.0.1:8000/error/
    


### Token Authentication for api
    Middleware
    `login.middleware.TokenAuthMiddleware`
    ```  
     Header params:

    `Authorization: TOKEN <token>` 

    Response

    ```
    

    