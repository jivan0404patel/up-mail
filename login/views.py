# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.views import View
from django.views.generic import TemplateView, ListView
from django.contrib.auth import authenticate, login, logout
from django_q.tasks import async_task
from rest_framework.authtoken.models import Token

from inbox.models import MessageModel
from inbox.tasks import messages_list


# this end point is for login page
class LoginView(TemplateView):
    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        messages.add_message(request,
                             message="Any personal data collected from this signup process will be delete after sometime.",
                             level=0)

        """ if user is logedin then redirect user to home page
        else render login page to user
        """
        if request.user.is_authenticated:
            return HttpResponseRedirect("/home/")
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    # login user on post request
    def post(self, request, *args, **kwargs):
        """authenticate user with email and password and redirect to home page """
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, email=email, password=password)
        if user:
            login(request, user=user)
            return HttpResponseRedirect('/home/')
        else:
            context = self.get_context_data(**kwargs)
            context['error'] = "Please Enter Valid Email Password"
            return self.render_to_response(context)


class LogOut(View):
    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            logout(request)
            return HttpResponseRedirect("/login/")


def signup_redirect(request):
    return HttpResponseRedirect("/home/")


class LoginRedirect(TemplateView):
    template_name = "sync_progress.html"

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseRedirect("/login/")
        async_task(messages_list, task_name="get_mail " + request.user.username, user=request.user)
        try:
            Token.objects.create(user=request.user)
        except Exception as e:
            print("error", str(e))

        if MessageModel.objects.filter(user=request.user).count() == 0:
            context = self.get_context_data(**kwargs)
            return self.render_to_response(context)
        return HttpResponseRedirect("/home/")
