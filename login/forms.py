from django import forms


class LoginFrom(forms.Form):
    email = forms.EmailField(label='Enter email', max_length=100)
    password = forms.CharField(label='password', max_length=100)
