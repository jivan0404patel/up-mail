from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from rest_framework.authtoken.models import Token


class AuthRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        if not request.user.is_authenticated and 'login' not in request.build_absolute_uri() and 'admin' not in request.build_absolute_uri():
            return HttpResponseRedirect('/login')  # or http response

        response = self.get_response(request)

        # if request.user.is_authenticated and 'home' not in request.build_absolute_uri():
        #     return HttpResponseRedirect('/home')
        # Code to be executed for each request/response after
        # the view is called.

        return response


class TokenAuthMiddleware:
    """
    Token authorization middleware for Django Channels 2
    """

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        headers = dict(request.headers)
        if 'Authorization' in headers:
            try:
                token_name, token_key = headers['Authorization'].split(" ")
                if token_name == 'Token':
                    token = Token.objects.get(key=token_key)
                    request.user = token.user
            except Token.DoesNotExist as e:
                print("error", e)
                pass

        return self.get_response(request)
