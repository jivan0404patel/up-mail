from django.urls import re_path

from .views import LoginView, LogOut, signup_redirect, LoginRedirect

app_name = 'login'
STATIC_ROOT = 'Static'

urlpatterns = [
    re_path(r'login/', LoginView.as_view(), name="login"),
    re_path(r'logout/', LogOut.as_view(), name="login"),
    re_path(r'social/signup/',signup_redirect, name="signup_redirect"),
    re_path(r'login_redirect/', LoginRedirect.as_view(), name="login_redirect"),
]
