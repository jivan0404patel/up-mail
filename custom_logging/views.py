from django.shortcuts import render
from django.views import View
from django.views.generic import TemplateView

from django.http import *


# Create your views here.


class LogError(View):

    def get(self, request, *args, **kwargs):
        error_code = request.GET.get("error_code", "")

        raise HttpResponseBadRequest()
