from django.contrib import admin

from custom_logging.models import ErrorCodeLogModel

# Register your models here.

admin.site.register(ErrorCodeLogModel)