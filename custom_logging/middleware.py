import logging
import traceback

from django.http import HttpResponseRedirect, HttpResponseNotFound
from django.conf import settings
import warnings

from custom_logging.models import ErrorCodeLogModel

logger = logging.getLogger("django.request")


def save_stack_track(store_response, status_code, user, url):
    try:
        save_response = ErrorCodeLogModel.objects.create(message=str(store_response), status_code=status_code, user=user, url=url)
        save_response.save()
    except Exception as e:
        logger.error(e.__str__())


def custom_logging_error_code():
    error_code = getattr(settings, "CUSTOM_LOGGING_ERROR_CODE", [])
    if type(error_code) is not list:
        warnings.warn('CUSTOM_LOGGING_ERROR_CODE should be of type list list not ' + str(type(list)))
        return []
    return error_code


class ExceptionLogMiddleware:
    def __init__(self, get_response):
        self.response = ""
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # call this VIEW IN flow
        self.response = self.get_response(request)
        print(self.response.status_code)
        return self.response

    def process_exception(self, request, exception):
        status_code = self.response.status_code
        print(self.response.status_code)
        if self.response.status_code in custom_logging_error_code():
            url = request.build_absolute_uri()
            logger.error(self.response)
            save_stack_track(''.join(traceback.format_tb(exception.__traceback__)), status_code, user=request.user,url=url)
            self.response.status_code = 200

