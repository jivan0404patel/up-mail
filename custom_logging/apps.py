from django.apps import AppConfig


class CustomLoggingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'custom_logging'
