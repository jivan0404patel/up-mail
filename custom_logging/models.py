from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class ErrorCodeLogModel(models.Model):
    status_code = models.CharField(max_length=150)
    message = models.TextField(default="", null=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, default=None, null=True)
    url = models.TextField(default="", null=True)
    created_at = models.DateField(auto_now=True, null=True)