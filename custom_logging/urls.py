from django.urls import re_path

from .views import LogError

app_name = 'login'
STATIC_ROOT = 'Static'

urlpatterns = [
    re_path(r'error/', LogError.as_view(), name="log error"),
]
