import base64
import json
from email.mime.text import MIMEText

import requests
from allauth.socialaccount.models import SocialToken
from django_q.tasks import async_task
from inbox.models import MessageModel, FetchedPageHistoryTokenModel
from inbox.models import MessageModel
import datetime


def messages_list(user):
    next_page_token = ""
    token: SocialToken = SocialToken.objects.get(account__user=user, account__provider='google')

    while next_page_token is not None:
        page_token = next_page_token
        url = "https://gmail.googleapis.com/gmail/v1/users/me/messages?maxResults=500&access_token=" + token.token
        if next_page_token:
            url += "&pageToken=" + next_page_token
        r = requests.get(url)
        data = json.loads(r.text)
        if 'error' in data:
            return "Error While Fetching Data"
        if 'nextPageToken' not in data:
            next_page_token = None
        else:
            next_page_token = data['nextPageToken']
        if page_token:
            try:
                FetchedPageHistoryTokenModel.objects.get(id=page_token, user=user)
                print("once token matched no need to check for rest other mails")
                return "Done"
            except Exception as e:
                print("error", str(e))
                pass

        for item in data['messages']:
            try:
                MessageModel.objects.get(id=item['id'])
                break
            except Exception as e:
                print('error', str(e))
                pass
            async_task(get_message, task_name=str(user.username) + "_" + str(user.id) + "_" + str(page_token),
                       page_token=page_token, message_id=item['id'],
                       user=user,
                       access_token=token.token, hook=page_fetched_hook)
    return user.username


def get_message(message_id, user, access_token, page_token):
    url = "https://gmail.googleapis.com/gmail/v1/users/me/messages/" + message_id + "?access_token=" + access_token
    response = requests.get(url)
    data = json.loads(response.text)
    if 'error' in data:
        return "Error While Fetching Data"
    message: MessageModel = MessageModel()
    message.id = message_id
    message.thread_id = data['threadId']
    message.label_ids = ','.join(data['labelIds'])
    message.snippet = data['snippet']
    message.payload = data['payload']
    message.size_estimate = data['sizeEstimate']
    message.history_id = data['historyId']
    message.internal_date = data['internalDate']
    message.user = user
    message.save()
    print('saved message_id', message_id)
    return page_token, user


def create_message(sender, to, subject, message_text):
    message = MIMEText(message_text)
    message['to'] = to
    message['from'] = sender
    message['subject'] = subject
    return {'raw': base64.urlsafe_b64encode(bytes(message))}


def sent_automated_mail(id, user):
    token: SocialToken = SocialToken.objects.get(account__user=user, account__provider='google')
    message: MessageModel = MessageModel.objects.get(id=id)
    message_params = create_message("jivan0404patel@gmail.com", "jp.dev.algo@gmail.com", "Hi",
                                    "Hello There, We will get back to you on this.")
    url = "https://gmail.googleapis.com/gmail/v1/users/{userId}/messages/send" + "?access_token=" + token.token
    response = requests.get(url, params=json.dumps({'raw': str(message_params)}))

    # TODO: not working so need to change api params

    message.auto_response = "DONE"
    message.save()
    print("send mail response ", response.text)
    return True


def page_fetched_hook(task):
    page_token, user = task.result
    if page_token:
        obj = FetchedPageHistoryTokenModel(id=page_token, user=user)
        obj.save()
    return ""
