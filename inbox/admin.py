from django.contrib import admin
from inbox.models import MessageModel, FetchedPageHistoryTokenModel

admin.site.register(MessageModel)
admin.site.register(FetchedPageHistoryTokenModel)
