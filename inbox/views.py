from django.http import HttpResponseRedirect

from django.views.generic import TemplateView
from rest_framework import generics, permissions

from inbox.models import MessageModel
from inbox.serializers import MessageModelSerializer
from inbox.tasks import sent_automated_mail


# Create your views here.
class HomePage(TemplateView, generics.ListAPIView):
    template_name = "home.html"

    """ for handling user_post request """
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = MessageModelSerializer

    def get(self, request, *args, **kwargs):
        """ if user is loged-in then redirect user to home page
        else render login page to user
        """
        if not request.user.is_authenticated:
            return HttpResponseRedirect("/login/")

        if request.GET.get("send_auto_response_id"):
            sent_automated_mail(request.GET.get("send_auto_response_id"), request.user)

        context = self.get_context_data(**kwargs)
        response = MessagesListAPIView.as_view()(request=request._request).data

        # for pagination
        response['offset'] = int(request.GET.get('offset', 0)) + 50

        # for iu
        context['data'] = response
        context['label'] = "ALL"
        context['url'] = request.build_absolute_uri()

        if '?' not in context['url']:
            context['url'] += "?"

        if request.GET.get("label"):
            context['label'] = request.GET.get("label")
        if request.GET.get("search"):
            context['search'] = request.GET.get("search")
        return self.render_to_response(context)


class MessagesListAPIView(generics.ListAPIView):
    """ for handling user_post request """
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = MessageModelSerializer

    def get_queryset(self):
        label = self.request.GET.get("label", "")
        search = self.request.GET.get("search", "")
        print("search", search)
        obj = MessageModel.objects.filter(user=self.request.user)
        if label and label != "ALL":
            obj = obj.filter(label_ids__icontains=label)
        if search:
            obj = obj.filter(snippet__icontains=search)
        print('obj.query.__str__()', obj.query.__str__())
        return obj
