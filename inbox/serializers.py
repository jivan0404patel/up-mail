from django.contrib.auth.models import User

from inbox.models import MessageModel

from rest_framework import serializers
from rest_framework.serializers import ModelSerializer


class MessageModelSerializer(ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(allow_null=True, source="user.name",
                                                    queryset=User.objects.all())
    class Meta:
        model = MessageModel
        fields = '__all__'
