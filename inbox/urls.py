from django.urls import re_path


from .views import HomePage, MessagesListAPIView

app_name = 'home'
STATIC_ROOT = 'Static'

urlpatterns = [
    re_path(r'get_messages/', MessagesListAPIView.as_view(), name="home"),
    re_path(r'home/', HomePage.as_view(), name="home"),
]
