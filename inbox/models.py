from django.contrib.auth.models import User
from django.db import models
from django_prometheus.models import ExportModelOperationsMixin


# Create your models here.


class MessageModel(ExportModelOperationsMixin('message_model'),models.Model):
    id = models.CharField(max_length=50, unique=True, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, default=None, null=True)
    thread_id = models.CharField(max_length=50, default="")
    label_ids = models.TextField(default="")
    snippet = models.TextField(default="")
    auto_response = models.CharField(max_length=50, default="", null=True)
    size_estimate = models.CharField(max_length=50, default="")
    history_id = models.CharField(max_length=50, default="")
    internal_date = models.CharField(max_length=50, default="")
    payload = models.JSONField(default="")

    def __str__(self):
        return str(self.snippet)


class FetchedPageHistoryTokenModel(ExportModelOperationsMixin('fetched_page_history_token_model'),models.Model):
    id = models.CharField(max_length=50, unique=True, primary_key=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, default=None, null=True)