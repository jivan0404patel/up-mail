from django import template

register = template.Library()


@register.filter(name='get_count_info')
def get_count_info(dictionary):
    offset = dictionary['offset']
    count_info = (str(offset - 50) if offset else '0') + " - " + str(offset) + " of " + str(dictionary['count'])
    return count_info


@register.filter(name='get_key')
def get_key(dictionary, key):
    data = dictionary[key]
    return data


@register.filter(name='get_subject')
def get_subject(dictionary):
    payload = dictionary['payload']
    headers = payload['headers']
    # Look for Subject and Sender Email in the headers
    subject = ""
    for d in headers:
        if d['name'] == 'Subject':
            subject = d['value']
        if d['name'] == 'From':
            sender = d['value']
    return subject


@register.filter(name='get_sender')
def get_sender(dictionary):
    payload = dictionary['payload']
    headers = payload['headers']
    # Look for Subject and Sender Email in the headers
    sender = ""
    for d in headers:
        if d['name'] == 'From':
            try:
                sender = d['value'][:d['value'].index("<")]
            except:
                sender = d['value']
    return sender


@register.filter(name='get_date')
def get_date(dictionary):
    payload = dictionary['payload']
    headers = payload['headers']
    # Look for Subject and Sender Email in the headers
    date = ""
    for d in headers:
        if d['name'] == 'Date':
            date = d['value'][4:11]
    return date
