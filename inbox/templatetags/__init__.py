def get_item(value): # Only one argument.
    """Converts a string into all lowercase"""
    return value.lower()